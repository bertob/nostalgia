<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop">
    <id>im.bernard.Nostalgia</id>
    <metadata_license>CC0-1.0</metadata_license>
    <project_license>GPL-3.0-or-later</project_license>
    <launchable type="desktop-id">im.bernard.Nostalgia.desktop</launchable>
    <name>Nostalgia</name>
    <summary>Set historic GNOME wallpapers</summary>
    <description>
        <p>
        Wallow in nostalgia and relive fond memories of GNOME releases past.
        </p>
    </description>
    <provides>
        <binary>nostalgia</binary>
    </provides>
    <requires>
        <display_length compare="ge">360</display_length>
    </requires>
    <recommends>
        <control>keyboard</control>
        <control>pointing</control>
        <control>touch</control>
    </recommends>
    <screenshots>
        <screenshot type="default">
            <image height="772" width="722">https://gitlab.gnome.org/bertob/nostalgia/raw/master/data/screenshots/screenshot.png</image>
            <caption>List of wallpapers</caption>
        </screenshot>
    </screenshots>
    <releases>
        <release version="0.10" date="2024-03-20">
            <description>
                <p>Add new wallpapers. Use newer widgets from libadwaita.</p>
            </description>
        </release>
        <release version="0.9" date="2022-01-12">
            <description>
                <p>Port to GTK4 and Libadwaita.</p>
            </description>
        </release>
        <release version="0.8" date="2021-08-11">
            <description>
                <p>Simplifies the About button and adds hardware support metadata.</p>
            </description>
        </release>
        <release version="0.7" date="2021-05-24">
            <description>
                <p>Uses the wallpaper portal, so we need fewer permissions.</p>
            </description>
        </release>
        <release version="0.6" date="2020-12-20">
            <description>
                <p>Introduces a sleek new transparent headerbar, switches the sorting order to recent-first, and adds the wallpaper from the latest GNOME release.</p>
            </description>
        </release>
        <release version="0.5" date="2020-06-14">
            <description>
                <p>Improves the visual presenation of wallpaper thumbnails and simplifies the menu to just an About button.</p>
            </description>
        </release>
        <release version="0.4" date="2020-02-16">
            <description>
                <p>Adds support for dynamic wallpapers that change throughout the day, and the new wallpapers from the most recent GNOME releases.</p>
            </description>
        </release>
        <release version="0.3" date="2018-12-01" />
        <release version="0.2" date="2018-11-26" />
        <release version="0.1" date="2018-11-22" />
    </releases>
    <url type="homepage">https://gitlab.gnome.org/bertob/nostalgia</url>
    <url type="bugtracker">https://gitlab.gnome.org/bertob/nostalgia/issues</url>
    <url type="vcs-browser">https://gitlab.gnome.org/bertob/nostalgia</url>
    <update_contact>tbernard@gnome.org</update_contact>
    <content_rating type="oars-1.1" />
    <translation type="gettext">nostalgia</translation>
    <!-- developer_name tag deprecated with Appstream 1.0 -->
    <developer_name>Tobias Bernard</developer_name>
    <developer id="im.bernard">
        <name>Tobias Bernard</name>
    </developer>
    <branding>
      <color type="primary" scheme_preference="light">#99c1f1</color>
      <color type="primary" scheme_preference="dark">#1a5fb4</color>
    </branding>
</component>


