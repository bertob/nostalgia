/* border-scrollable.vala
 *
 * Copyright 2021 Alexander Mikhaylenko
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Nostalgia.BorderScrollable : Adw.Bin, Gtk.Scrollable {
    public Gtk.Widget header { get; set; }
    public int header_height { get; private set; }

    public Gtk.Adjustment vadjustment { get; set construct; }
    public Gtk.Adjustment hadjustment { get; set construct; }
    public Gtk.ScrollablePolicy vscroll_policy { get; set; }
    public Gtk.ScrollablePolicy hscroll_policy { get; set; }

    private Binding? bind_vadjustment;
    private Binding? bind_hadjustment;
    private Binding? bind_vscroll_policy;
    private Binding? bind_hscroll_policy;

    construct {
        notify["child"].connect (() => {
            unbind_properties ();

            if (child is Gtk.Scrollable)
                bind_properties ();
        });
    }

    static construct {
        set_css_name ("border-scrollable");
    }

    protected bool get_border (out Gtk.Border border) {
        if (!(child is Gtk.Scrollable)) {
            border = {};
            return false;
        }

        var scrollable = child as Gtk.Scrollable;

        if (scrollable != null)
            scrollable.get_border (out border);
        else
            border = {};

        header_height = header.get_height ();
        border.top += (int16) header_height;

        return true;
    }

    private void bind_properties () {
        bind_vadjustment = bind_property ("vadjustment",
                                          child,
                                          "vadjustment",
                                          BindingFlags.SYNC_CREATE);
        bind_hadjustment = bind_property ("hadjustment",
                                          child,
                                          "hadjustment",
                                          BindingFlags.SYNC_CREATE);
        bind_vscroll_policy = bind_property ("vscroll-policy",
                                             child,
                                             "vscroll-policy",
                                             BindingFlags.SYNC_CREATE);
        bind_hscroll_policy = bind_property ("hscroll-policy",
                                             child,
                                             "hscroll-policy",
                                             BindingFlags.SYNC_CREATE);
    }

    private void unbind_properties () {
        if (bind_vadjustment != null) {
            bind_vadjustment.unbind ();
            bind_vadjustment = null;
        }
        if (bind_hadjustment != null) {
            bind_hadjustment.unbind ();
            bind_hadjustment = null;
        }
        if (bind_vscroll_policy != null) {
            bind_vscroll_policy.unbind ();
            bind_vscroll_policy = null;
        }
        if (bind_hscroll_policy != null) {
            bind_hscroll_policy.unbind ();
            bind_hscroll_policy = null;
        }
    }
}
