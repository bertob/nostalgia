/* window.vala
 *
 * Copyright 2018 Tobias Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Nostalgia {
    [GtkTemplate (ui = "/org/gnome/Nostalgia/window.ui")]
    public class Window : Adw.ApplicationWindow {

        [GtkChild]
        private unowned Gtk.ListBox list;

        public Window (Gtk.Application app) {
            Object (application: app);
        }

        public int wallpaper_width { get; set; }
        public int wallpaper_height { get; set; }

        construct {
            add_dynamic_wallpaper ("48", "48", "March 15, 2025");
            add_dynamic_wallpaper ("47", "47", "September 18, 2024");
            add_dynamic_wallpaper ("46", "46", "March 20, 2024");
            add_dynamic_wallpaper ("45", "45", "September 20, 2023");
            add_dynamic_wallpaper ("44", "44", "March 22, 2023");
            add_dynamic_wallpaper ("43", "43", "September 21, 2022");
            add_dynamic_wallpaper ("42", "42", "March 23, 2022");
            add_dynamic_wallpaper ("41", "41", "September 22, 2021");
            add_dynamic_wallpaper ("40", "40", "March 24, 2021");
            add_dynamic_wallpaper ("3-38", "3.38", "September 16, 2020");
            add_dynamic_wallpaper ("3-36", "3.36", "March 11, 2020");
            add_dynamic_wallpaper ("3-34", "3.34", "September 12, 2019");
            add_dynamic_wallpaper ("3-32", "3.32", "March 13, 2019");
            add_dynamic_wallpaper ("3-30", "3.30", "September 5, 2018");
            add_dynamic_wallpaper ("3-28", "3.28", "March 14, 2018");
            add_dynamic_wallpaper ("3-26", "3.26", "September 13, 2017");
            add_dynamic_wallpaper ("3-24", "3.24", "March 22, 2017");
            add_dynamic_wallpaper ("3-22", "3.22", "September 21, 2016");
            add_dynamic_wallpaper ("3-20", "3.20", "March 23, 2016");
            add_dynamic_wallpaper ("3-18", "3.18", "September 23, 2015");
            add_dynamic_wallpaper ("3-16", "3.16", "March 25, 2015");
            add_dynamic_wallpaper ("3-14", "3.14", "September 24, 2014");
            add_dynamic_wallpaper ("3-12", "3.12", "March 26, 2014");
            add_dynamic_wallpaper ("3-10", "3.10", "September 25, 2013");
            add_dynamic_wallpaper ("3-6", "3.8", "March 27, 2013");        // identical to 3.6
            add_dynamic_wallpaper ("3-6", "3.6", "September 26, 2012");
            add_dynamic_wallpaper ("3-4", "3.4", "March 28, 2012");
            add_static_wallpaper ("3-0.jpg", "3.2", "September 28, 2011"); // identical to 3.0
            add_static_wallpaper ("3-0.jpg", "3.0", "April 6, 2011");
        }

        public void add_static_wallpaper (string filename, string version, string date) {
            var wallpaper_row = new WallpaperRow.for_static (
                filename,
                version,
                date,
                true,
                this.wallpaper_width,
                this.wallpaper_height
            );
            this.bind_property (
                "wallpaper-width",
                wallpaper_row,
                "wallpaper-width",
                BindingFlags.DEFAULT
            );
            this.bind_property (
                "wallpaper-height",
                wallpaper_row,
                "wallpaper-height",
                BindingFlags.DEFAULT
            );
            list.append (wallpaper_row);
        }

        public void add_dynamic_wallpaper (string dir, string version, string date) {
            var wallpaper_row = new WallpaperRow.for_dynamic (
                dir,
                version,
                date,
                false,
                this.wallpaper_width,
                this.wallpaper_height
            );
            this.bind_property (
                "wallpaper-width",
                wallpaper_row,
                "wallpaper-width",
                BindingFlags.DEFAULT
            );
            this.bind_property (
                "wallpaper-height",
                wallpaper_row,
                "wallpaper-height",
                BindingFlags.DEFAULT
            );
            list.append (wallpaper_row);
        }

        [GtkCallback]
        private void row_activated_cb (Gtk.ListBoxRow row) {
            assert (row is WallpaperRow);

            var wallpaper_row = row as WallpaperRow;

            if (wallpaper_row.is_static)
                set_static_wallpaper.begin (wallpaper_row.file);
            else {
                var dir = wallpaper_row.file.get_parent ();
                set_dynamic_wallpaper.begin (dir);
            }
        }

        [GtkCallback]
        private void breakpoint_apply_cb (Adw.Breakpoint row) {
            add_css_class ("narrow-display");
        }

        [GtkCallback]
        private void breakpoint_unapply_cb (Adw.Breakpoint row) {
            remove_css_class ("narrow-display");
        }

        private async void set_static_wallpaper (File file) {
            var name = "wallpaper.jpg";

            try {
                yield copy_file (file, name);
            } catch (Error e) {
                warning ("Could not copy static wallpaper: %s", e.message);
            }
            try {
                yield set_wallpaper (name);
            } catch (Error e) {
                warning ("Could not set static wallpaper: %s", e.message);
            }
        }

        private async void set_dynamic_wallpaper (File dir) {
            try {
                yield copy_file (dir.get_child ("day.jpg"), "day.jpg");
                yield copy_file (dir.get_child ("morning.jpg"), "morning.jpg");
                yield copy_file (dir.get_child ("night.jpg"), "night.jpg");
                yield copy_xml (dir.get_basename ());
            } catch (Error e) {
                warning ("Could not copy dynamic wallpapers: %s", e.message);
            }
            try {
                yield set_wallpaper (dir.get_basename () + ".xml");
            } catch (Error e) {
                warning ("Could not set dynamic wallpaper: %s", e.message);
            }
        }

        private async string read_file (string filename) throws Error {
            var file = File.new_for_path (filename);

            var @is = file.read ();
            var dis = new DataInputStream (@is);
            var line = "";
            var contents = "";

            while ((line = yield dis.read_line_async ()) != null)
                contents += line + "\n";

            yield dis.close_async ();

            return contents;
        }

        private async void copy_xml (string version) throws Error {
            var dest_dir = Environment.get_user_data_dir ();

            var contents = yield read_file (Config.WALLPAPERS_DIR + "/wallpaper-timed.xml");
            contents = contents.replace ("@datadir@", dest_dir);
            contents = contents.replace ("@morning@", "morning");
            contents = contents.replace ("@day@", "day");
            contents = contents.replace ("@night@", "night");

            var dest = File.new_for_path (dest_dir + "/" + version + ".xml");
            yield dest.replace_contents_async (contents.data, null, false, FileCreateFlags.NONE, null, null);
        }

        private async void copy_file (File src, string destname) throws Error {

            var dest = File.new_for_path (Environment.get_user_data_dir () + "/" + destname);

            message (@"Saving $(src.get_basename ()) as $(dest.get_path ())");

            yield src.copy_async (dest, FileCopyFlags.OVERWRITE, Priority.DEFAULT_IDLE);
        }

        private async void set_wallpaper (string filename) throws Error {
            var portal = new Xdp.Portal.initable_new ();
            var file = File.new_build_filename (Environment.get_user_data_dir (), filename);

            yield portal.set_wallpaper (
                Xdp.parent_new_gtk (this),
                file.get_uri (),
                Xdp.WallpaperFlags.BACKGROUND,
                null
            );
        }

          public void show_about () {
            string[] developers = {
                "Tobias Bernard <tbernard@gnome.org>",
                "Felix Häcker <haecker.felix1207@gmail.com>",
                "Bilal Elmoussaoui <bil.elmoussaoui@gmail.com>",
                "Alexander Mikhaylenko <exalm7659@gmail.com>",
                "Forever XML <foreverxml@tuta.io>",
                "Andrei-Costin Zisu <azisu@gnome.org>"
            };
            string[] designers = {
                "Tobias Bernard <tbernard@gnome.org>"
            };
            string[] artists = {
                "Jakub Steiner <jimmac@gmail.com>"
            };

            var about = new Adw.AboutDialog () {
                application_name = "Nostalgia",
                application_icon = "im.bernard.Nostalgia",
                version = "0.10",
                copyright = "Copyright © 2018 Tobias Bernard",
                license_type = Gtk.License.GPL_3_0,
                developers = developers,
                designers = designers,
                artists = artists,
                translator_credits = _("translator-credits"),
                website = "https://gitlab.gnome.org/bertob/nostalgia"
              };

              about.present (this);
          }

    }
}
