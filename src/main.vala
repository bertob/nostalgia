/* main.vala
 *
 * Copyright 2018 Tobias Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


const GLib.ActionEntry[] ACTION_ENTRIES = {
    { "quit", on_quit_activate },
};

void on_quit_activate () {
    var app = (!) GLib.Application.get_default ();
    app.quit ();
}

int main (string[] args) {
    var app = new Adw.Application ("im.bernard.Nostalgia", ApplicationFlags.DEFAULT_FLAGS);
    GLib.Environment.set_application_name ("Nostalgia");
    app.set_resource_base_path ("/org/gnome/Nostalgia");
    var action = new GLib.SimpleAction ("about", null);
    app.add_action (action);

    Gtk.Window.set_default_icon_name ("im.bernard.Nostalgia");

    app.startup.connect (() => {
        var style_manager = Adw.StyleManager.get_default ();
        style_manager.set_color_scheme (PREFER_DARK);

        app.add_action_entries (ACTION_ENTRIES, app);
        app.set_accels_for_action ("app.quit", { "<Control>q" });
        app.set_accels_for_action ("window.close", { "<Control>w" });
    });

    app.activate.connect (() => {
        var win = app.active_window;
        if (win == null) {
            win = new Nostalgia.Window (app);
            action.activate.connect (() => {
                ((Nostalgia.Window) win).show_about ();
            });
        }
        win.present ();
    });

    typeof (Nostalgia.BorderScrollable).ensure ();

    return app.run (args);
}
