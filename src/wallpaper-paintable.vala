/* wallpaper-paintable.vala
 *
 * Copyright 2021 Alexander Mikhaylenko
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Nostalgia {
    public class WallpaperPaintable : Object, Gdk.Paintable {
        public Gdk.Texture texture { get; construct; }

        public WallpaperPaintable (Gdk.Texture texture) {
            Object (texture: texture);
        }

        public void snapshot (Gdk.Snapshot gdk_snapshot, double width, double height) {
            var snapshot = gdk_snapshot as Gtk.Snapshot;

            double x, y, w, h;
            double ratio = width / height;
            double texture_ratio = texture.get_intrinsic_aspect_ratio ();

            if (texture_ratio > ratio) {
                h = height;
                w = width * texture_ratio / ratio;
                x = (width - w) / 2;
                y = 0;
            } else {
                h = height / texture_ratio * ratio;
                w = width;
                x = 0;
                y = (height - h) / 2;
            }

            snapshot.translate ({ (float) x, (float) y});
            texture.snapshot (gdk_snapshot, w, h);
        }
    }
}
